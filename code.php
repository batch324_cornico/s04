<?php 

class Building {
	//if the access modifier of the property is private, you cannot directly access its value

	//if the access modifier is private the child class won't inherit the properties
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	//getter function of property name
	public function getName(){
		return $this->name;
	}

	//getter function of property floors
	public function getFloors(){
		return $this->floors;
	}

	//getter function of property address
	public function getAddress(){
		return $this->address;
	}

	//setter function of property name
	public function setName($name){
		$this->name = $name;
	}

}

class Condominium extends Building{

}


$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avanue, Makati City, Philippines');





 ?>
