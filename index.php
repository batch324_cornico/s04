<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<title>S04:Activity</title>
	</head>

	<body>

		<h1>Building</h1>
		<!-- Getter for name only-->
		<p><?php echo "The name of the building is " . $building->getName() . "." ; ?></p>
		<!-- Getter for name and floors-->
		<p><?php echo "The " . $building->getName() . " has " . $building->getFloors() ." floors." ; ?></p>
		<!-- Getter for name and address-->
		<p><?php echo "The " . $building->getName() . " is located at " . $building->getAddress() ."." ; ?></p>
		<!-- Setter -->
		<?php $building->setName('Caswynn Complex'); ?>
		<!-- Getter for name only-->
		<p><?php echo "The name of the building has been changed to " . $building->getName() . "." ; ?></p>







		<h1>Condominium</h1>
		<!-- Getter for name only-->
		<p><?php echo "The name of the condominium is " . $condominium->getName() . "." ; ?></p>
		<!-- Getter for name and floors-->
		<p><?php echo "The " . $condominium->getName() . " has " . $condominium->getFloors() ." floors." ; ?></p>
		<!-- Getter for name and address-->
		<p><?php echo "The " . $condominium->getName() . " is located at " . $condominium->getAddress() ."." ; ?></p>
		<!-- Setter -->
		<?php $condominium->setName('Enzo Tower'); ?>
		<!-- Getter for name only-->
		<p><?php echo "The name of the condominium has been changed to " . $condominium->getName() . "." ; ?></p>

		
	</body>
</html>
